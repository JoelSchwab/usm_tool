# Urban Sprawl Metrics Toolset

## Installation

The installation of the Urban Sprawl (USL) Processing algorithmen set can be done with the plugin manager.
1. Open your QGIS application
2. Click on the `Plugins` option in the Toolbar and from there open `Manage and install plugins...`
3. Use the option `Install from zip` and use the previously downloaded zip File.
4. The set of processing algorithmen should now appear in the processing toolbox.

## Operating instructions

### Calculate Weighted Urban Proliferation

`Built-up area (Raster)`: The raster with the settlement are. For a more accurate calculation the settlement area should go beyond the area boundary.

`Reporting unit (Vector)`: The area for which the urban sprawl should be calculated

`Identifier`: Identifier for the area

`Inhabitants`: Amount of inhabitants in the area

`Employees`: Amount of employees in the area

`Share of settlement area`: Amount of habitable area

`Output folder`: Where the results should be saved to


### Explore Weighted Urban Proliferation

`Reporting unit (Vector)`: The area for which the urban sprawl should be calculated

`Urban dispersion (DIS)`: Value of the already calculated DIS value. The value can be calculated with the other tool

`Settlement area`: Size of the settlement area

`Inhabitants`: Amount of inhabitants in the area

`Employees`: Amount of employees in the area

`Share of settlement area`: Amount of habitable area

`Output folder`: Where the results should be saved to