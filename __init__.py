# noinspection PyPep8Naming
# type: ignore
def classFactory(iface):  # pylint: disable=invalid-name
    from .urban_sprawl_plugin import UrbanSprawlPlugin
    return UrbanSprawlPlugin()
