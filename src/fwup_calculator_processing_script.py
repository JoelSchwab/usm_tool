from typing import Dict, Any, Optional

from qgis.PyQt.QtCore import QCoreApplication
from qgis._core import (
    QgsProcessingParameterVectorLayer,
    QgsVectorLayer,
    QgsProcessingParameterFolderDestination,
    QgsProcessingParameterField,
)
from qgis.core import (
    QgsProcessingContext,
    QgsProcessingFeedback,
    QgsProcessingAlgorithm,
    edit,
    QgsVectorFileWriter,
    QgsProject,
    Flags,

)

from .urban_sprawl.clip_raster.raster_clipper import RasterClipper
from .urban_sprawl.common import constants
from .wup_processing import (
    calculate_and_save_wdis,
    calculate,
    get_output_path,
)


class FastWupCalculatorProcessingScript(QgsProcessingAlgorithm):  # type: ignore
    @staticmethod
    def tr(string: str) -> str:
        return QCoreApplication.translate("Processing", string)  # type: ignore

    def createInstance(self) -> "FastWupCalculatorProcessingScript":
        return FastWupCalculatorProcessingScript()

    def name(self) -> str:
        return "usm_fwup_calculator"

    def displayName(self) -> str:
        return self.tr("Explore Weighted Urban Proliferation")

    def flags(self) -> Flags:
        return super().flags() | QgsProcessingAlgorithm.FlagNoThreading

    def group(self) -> str:
        return self.tr(constants.GROUP_NAME)

    def groupId(self) -> str:
        return constants.GROUP_ID

    def helpUrl(self) -> str:
        return (
            "https://gitlab.com/ba-qgis/documents/-/blob/master/documentation/doku.pdf"
        )

    def shortHelpString(self) -> str:
        return self.tr(
            'The results from the tool "Calculate Weighted Urban Proliferation" serve as the basis for this tool. The '
            "input variables can be changed manually and the indicators recalculated. In this way, changes can be "
            "checked quickly. "
        )

    def initAlgorithm(self, _: Optional[Dict[str, Any]] = None) -> None:  # type: ignore

        self.addParameter(
            QgsProcessingParameterVectorLayer(
                constants.VECTOR, self.tr("Inputlayer (Vector or Table)")
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.DIS_FIELD,
                self.tr("Urban dispersion (DIS)"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="Dis",
                optional=True,
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.SETTLEMENT_AREA,
                self.tr("Settlement area"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="settlement area",
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.INHABITANT_FIELD,
                self.tr("Inhabitants"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="Inhabitants",
                optional=True,
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.EMPLOYEE_FIELD,
                self.tr("Employees"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="Employees",
                optional=True,
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.SSA_FIELD,
                self.tr("Share of settlement area (SSA)"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="SSA_Share",
                optional=True,
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                constants.OUTPUT, self.tr("Output Folder")
            )
        )

    def processAlgorithm(
            self,
            parameters: dict[str, object],
            context: QgsProcessingContext,
            feedback: QgsProcessingFeedback,
    ) -> dict[str, str]:

        vector: QgsVectorLayer = self.parameterAsVectorLayer(
            parameters, constants.VECTOR, context
        )

        output_path = self.parameterAsString(parameters, constants.OUTPUT, context)

        gpkg_path = get_output_path(parameters, output_path, "_wup_result.gpkg")

        vector = RasterClipper.copy_vector_layer(context, vector)
        QgsProject.instance().addMapLayer(vector)

        RasterClipper.create_fields(vector)
        vector.updateFields()

        with edit(vector):
            for feat in vector.getFeatures():
                feedback.pushInfo("Preparing Data")

                if feedback.isCanceled():
                    return {}

                if (
                        parameters[constants.DIS_FIELD]
                        and feat[parameters[constants.DIS_FIELD]]
                ):
                    dis_value = feat[parameters[constants.DIS_FIELD]]
                    calculate_and_save_wdis(dis_value, feat, vector)
                else:
                    return {}

                if (
                        parameters[constants.SETTLEMENT_AREA]
                        and feat[parameters[constants.SETTLEMENT_AREA]]
                ):
                    build_up_area = feat[parameters[constants.SETTLEMENT_AREA]]
                else:
                    feedback.reportError("Settlement Area needs to be added")

                calculate(feat, vector, build_up_area, dis_value, feedback, parameters)
                QgsVectorFileWriter.writeAsVectorFormat(vector, gpkg_path, "utf-8")

        feedback.setProgress(100)
        return {constants.OUTPUT: output_path}
