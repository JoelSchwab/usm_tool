from typing import Dict, Any, Optional

from PyQt5.QtCore import QVariant
from osgeo import gdal
from qgis.PyQt.QtCore import QCoreApplication
from qgis._core import (
    QgsProcessingParameterVectorLayer,
    QgsVectorLayer,
    QgsProcessingParameterFolderDestination,
    QgsProcessingParameterField,
    QgsProject,
    QgsDataProvider,
    QgsField,
    QgsVectorFileWriter,
)
from qgis.core import (
    QgsProcessingContext,
    QgsProcessingFeedback,
    QgsProcessingAlgorithm,
    QgsProcessingParameterRasterLayer,
    edit,
    Flags,
)

from .urban_sprawl.clip_raster.raster_clipper import RasterClipper
from .urban_sprawl.common import constants
from .urban_sprawl.common.common import Common
from .wup_processing import (
    get_si_raster,
    calculate_and_save_dis,
    save_raster,
    get_si_lib,
    get_output_path,
    calculate_and_save_wdis,
    calculate,
)


class WupCalculatorProcessingScript(QgsProcessingAlgorithm):  # type: ignore
    @staticmethod
    def tr(string: str) -> str:
        return QCoreApplication.translate("Processing", string)  # type: ignore

    def createInstance(self) -> "WupCalculatorProcessingScript":
        return WupCalculatorProcessingScript()

    def name(self) -> str:
        return "usm_wup_calculator"

    def displayName(self) -> str:
        return self.tr("Calculate Weighted Urban Proliferation")

    def flags(self) -> Flags:
        return super().flags() | QgsProcessingAlgorithm.FlagNoThreading

    def group(self) -> str:
        return self.tr(constants.GROUP_NAME)

    def groupId(self) -> str:
        return constants.GROUP_ID

    def helpUrl(self) -> str:
        return (
            "https://gitlab.com/ba-qgis/documents/-/blob/master/documentation/doku.pdf"
        )

    def shortHelpString(self) -> str:
        return self.tr(
            'This tool calculates the "Weigthed Urban Proliferation" by Jaeger, Schwick. The analysis '
            "calculates the weighted urban proliferation based on the dispersion of the built-up area, "
            "the inhabitants and jobs for one or more reporting unit. Find more literature and test "
            "datasets here:"
        )

    def initAlgorithm(self, _: Optional[Dict[str, Any]] = None) -> None:  # type: ignore
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                constants.RASTER, self.tr("Built-up area (Raster)")
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterVectorLayer(
                constants.VECTOR, self.tr("Reporting unit (Vector)")
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                constants.OUTPUT, self.tr("Output folder")
            )
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.NAME_FIELD,
                self.tr("Identifier"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="name",
                optional=True,
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.INHABITANT_FIELD,
                self.tr("Inhabitants"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="Inhabit",
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.EMPLOYEE_FIELD,
                self.tr("Employees"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="Employees",
                optional=True,
            ),
            createOutput=False,
        )

        self.addParameter(
            QgsProcessingParameterField(
                constants.SSA_FIELD,
                self.tr("Share of settlement area"),
                parentLayerParameterName=constants.VECTOR,
                defaultValue="SSA_Share",
                optional=True,
            ),
            createOutput=False,
        )

    def processAlgorithm(
        self,
        parameters: dict[str, object],
        context: QgsProcessingContext,
        feedback: QgsProcessingFeedback,
    ) -> dict[str, str]:

        raster_path = self.parameterAsRasterLayer(
            parameters, constants.RASTER, context
        ).source()
        output_path = self.parameterAsString(parameters, constants.OUTPUT, context)

        vector: QgsVectorLayer = self.parameterAsVectorLayer(
            parameters, constants.VECTOR, context
        )

        gpkg_path = get_output_path(parameters, output_path, "_wup_result.gpkg")

        vector = RasterClipper.copy_vector_layer(context, vector)
        QgsProject.instance().addMapLayer(vector)
        layer_provider: QgsDataProvider = vector.dataProvider()

        # Add DIS field if it doesnt exist
        dis_index = vector.fields().indexOf("Dis")
        if dis_index == -1:
            layer_provider.addAttributes([QgsField("Dis", QVariant.Double)])
        settlement_area_index = vector.fields().indexOf("settlement_area")
        if settlement_area_index == -1:
            layer_provider.addAttributes([QgsField("settlement_area", QVariant.Double)])

        RasterClipper.create_fields(vector)

        vector.updateFields()

        si_lib = get_si_lib()

        if si_lib is None:
            feedback.reportError(str("Could not load the lib"))
            return {}

        raster = gdal.Open(raster_path)
        pixel_size = Common.get_pixel_size(raster)
        all_features = list(vector.getFeatures())
        original_len = len(all_features)
        count = 0

        with edit(vector):
            for feat in all_features:
                count += 1
                if count == original_len * 1.5:
                    break
                clipped_matrix, clipped_raster, result_matrix = get_si_raster(
                    feat,
                    feedback,
                    parameters,
                    pixel_size,
                    raster,
                    si_lib,
                    vector,
                    output_path,
                )

                if clipped_raster is None:
                    feedback.reportError(
                        "Invalid Geometry for " + str(feat[parameters[self.NAME_FIELD]])
                    )
                    continue

                if feedback.isCanceled():
                    return {}

                save_raster(
                    clipped_raster,
                    feat,
                    output_path,
                    parameters,
                    result_matrix,
                )

                # Calculate DIS
                dis_value = calculate_and_save_dis(
                    feat, feedback, result_matrix, vector
                )
                if dis_value == -1:
                    all_features.append(feat)
                    continue

                calculate_and_save_wdis(dis_value, feat, vector)

                build_up_area = Common.get_area(
                    clipped_matrix,
                    pixel_size,
                    lambda x: x == constants.BUILD_UP_VALUE,
                )

                calculate(feat, vector, build_up_area, dis_value, feedback, parameters)
                QgsVectorFileWriter.writeAsVectorFormat(vector, gpkg_path, "utf-8")

                feedback.setProgress(((count / original_len) * 100) - 10)

        feedback.setProgress(100)
        return {constants.OUTPUT: output_path}
