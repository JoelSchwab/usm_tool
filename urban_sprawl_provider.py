# type: ignore
from typing import Any

from qgis.core import QgsProcessingProvider

from .src.urban_sprawl.common import constants
from .src.wup_calculator_processing_script import WupCalculatorProcessingScript
from .src.fwup_calculator_processing_script import FastWupCalculatorProcessingScript


class UrbanSprawlProvider(QgsProcessingProvider):  # type: ignore
    def __init__(self):
        QgsProcessingProvider.__init__(self)

    def unload(self) -> None:
        pass

    def loadAlgorithms(self) -> None:
        self.addAlgorithm(WupCalculatorProcessingScript())
        self.addAlgorithm(FastWupCalculatorProcessingScript())

    def id(self) -> str:
        return constants.GROUP_ID

    def name(self) -> str:
        return self.tr(constants.GROUP_NAME)

    def icon(self) -> Any:
        return QgsProcessingProvider.icon(self)

    def longName(self) -> str:
        return self.name()
